<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<title>Fasilitas Transit Travel Ticket</title>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<link rel="stylesheet" href="css/skel.css" />
	<link rel="stylesheet" href="css/style.css" />
	<link rel="stylesheet" href="css/style-xlarge.css" />

	<link rel="icon" href="images/logo.png">
</head>

<body class="landing">

	<!-- Header -->
	<?php include "views/header.php"; ?>

	<!-- Banner -->
	<section id="banner">
		<h3>Fasilitas Kami</h3>
	</section>

	<!-- One -->
	<section id="two" class="wrapper style2 special">
		<div class="container">
			<header class="major">
				<h2>Tempat Parkir</h2>
				<br>
				<p align="justify">
					Memiliki lahan parkir yang mampu ditempati hingga 70 kendaraan roda empat dan 150 roda dua. Tempat aman untuk kendaraan anda yang sedang melakukan perjalanan jauh ber hari-hari hingga ber minggu-minggu.
				</p>
				<p align="justify">
					Kendaraan yang keluar masuk memiliki jalur masing-masing, sehingga tidak akan ada kemacetan yang tidak diperlukan ketika keluar masuk kendaraan ke dalam stasiun.
				</p>
			</header>
		</div>
	</section>

	<!-- Two -->
	<section id="two" class="wrapper style1 special">
		<div class="container">
			<header class="major">
				<h2>Ruang Tunggu Penumpang</h2>
				<br>
				<p align="justify">
					Penumpang yang akan melakukan perjalanan dapat menunggu Kereta yang akan ditumpangi dengan nyaman, karena kami memiliki bangku yang nyaman ketika anda bersandar, setiap pojok terdapat Charga Corner yang berguna mengisi ulang baterai gadget anda ketika sedang menunggu Kereta anda.
				</p>
			</header>
		</div>
	</section>

	<!-- Three -->
	<section id="two" class="wrapper style2 special">
		<div class="container">
			<header class="major">
				<h2>Kereta Api</h2>
				<br>
				<p align="justify">
					Transit Travel.
				</p>
			</header>
		</div>
	</section>

	<!-- Footer -->
	<?php include "views/footer.php"; ?>

</body>

<?php include "script.php"; ?>

</html>