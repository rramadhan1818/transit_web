<?php
session_start();

$form_error = '';

if (isset($_POST['submit'])) {

	$username = $_POST['username'];
	$password = $_POST['password'];

	if ($username == 'admin' and $password == '123') {
		$_SESSION['username'] = $username;
		header('Location: index.php');
	} else {
		$form_error = '* Password atau username yang kamu masukkan salah';
	}
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<title>Masuk Akun Transit Travel</title>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<script src="js/init.js"></script>
	<link rel="stylesheet" href="css/skel.css" />
	<link rel="stylesheet" href="css/style.css" />
	<link rel="stylesheet" href="css/alert.css" />
	<link rel="stylesheet" href="css/style-xlarge.css" />

<link rel="icon" href="images/logo.png">
</head>

<body>

	<?php include "views/header.php"; ?>

	<!-- One -->
	<section id="three" class="wrapper style1 special">
		<div class="container">
			<header class="major">
				<h2>Silahkan Masukan Akun Transit Travel Anda </h2>
			</header>
		</div>
		<div class="container 50%">
			<form action="#" method="post">
				<div class="row uniform">
					<div class="12u">
						<input name="username" id="username" value="" placeholder="Username/Email.." type="text">
					</div>
					<div class="12u">
						<input name="password" id="password" value="" placeholder="Password.." type="password">
						<p style="float:left; margin-top:15px;">Belum punya akun ? <a href="sign-up.php"><u>Register</u></a></p>
					</div>
					<center style="color:red;"><?php echo $form_error ?? ''; ?></center>

					<div class="12u$">
						<ul class="actions">
							<li><input value="MASUK" class="special big" type="submit" name="submit" onclick="checkError()"></li>
						</ul>
					</div>

				</div>
			</form>
		</div>
	</section>

	<!-- Footer (Test) -->
	<?php include "views/footer.php"; ?>

	<script>
		function checkError() {
			let username = document.getElementById("username").value;
			let password = document.getElementById("password").value;
			let errorData = "";
			let errorPassword = "";

			if (username == "" && password == "") {
				document.getElementById("username").style = "border: 1px solid red";
				document.getElementById("password").style = "border: 1px solid red";
				errorData = "username and password"
			}
			// else if(username == "admin@gmail.com" && password == "123"){
			// 	window.alert("Anda berasil login")
			//   }
			else if (username == "") {
				document.getElementById("username").style = "border: 1px solid red";
				errorData = "username"
			} else if (password == "") {
				document.getElementById("password").style = "border: 1px solid red";
				errorData = "password"
			}


			if (errorData != "") {
				window.alert("Data " + errorData + " tidak boleh kosong")
			} else {
				document.getElementById("username").style = "border: 1px solid black";
				document.getElementById("password").style = "border: 1px solid black";
			}

		}
	</script>
</body>

</html>