<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<title>Pesan Tiket Kereta Api Online di Transit Travel</title>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<script src="js/init.js"></script>
	<link rel="stylesheet" href="css/skel.css" />
	<link rel="stylesheet" href="css/style.css" />
	<link rel="stylesheet" href="css/style-xlarge.css" />

	<link rel="icon" href="images/logo.png">
</head>

<body>
	<?php include "views/header.php"; ?>

	<section id="main" class="wrapper">
		<div class="container 50%">
			<h2>
				<center>Pesan Tiket</center>
			</h2>
			<form method="post" action="#">
				<div class="row uniform 50%">
					<div class="12u 12u$(4)">
						<input type="text" name="name" id="name" value="" placeholder="Name" onFocus="sorot(this)" onBlur="hilang(this)" />
					</div>
					<div class="12u$ 12u$(4)">
						<input type="email" name="email" id="email" value="" placeholder="Email" onFocus="sorot(this)" onBlur="hilang(this)" />
					</div>
					<div class="12u$ 12u$(4)">
						<input type="text" name="nik" id="nik" value="" placeholder="No KTP" onFocus="sorot(this)" onBlur="hilang(this)" />
					</div>
					<div class="12u$ 12u$(4)">
						<input type="text" name="hp" id="hp" value="" placeholder="No HP" onFocus="sorot(this)" onBlur="hilang(this)" />
					</div>
					<div class="12u$ 12u$(4)">
						<div class="select-wrapper">
							<select name="lokasi" id="lokasi">
								<option selected="selected">Lokasi</option>
								<option value="1">Bandung - Jakarta</option>
								<option value="2">Jakarta - Bekasi</option>
								<option value="3">Bekasi - Tangerang</option>
								<option value="4">Tangerang - Bandung</option>
							</select>
						</div>
					</div>
					<div class="12u$ 12u$(4)">
						<input type="text" name="penumpang" id="penumpang" value="" placeholder="Jumlah Penumpang" onFocus="sorot(this)" onBlur="hilang(this)" />
					</div>
					<div class="12u$ 12u$(4)">
						<div class="select-wrapper">
							<select name="waktu" id="waktu">
								<option value="1">05.15 - 08.45</option>
								<option value="2">09.30 - 13.00</option>
								<option value="3">13.45 - 17.15</option>
								<option value="4">18.00 - 21.30</option>
								<option selected="selected">Waktu Keberangkatan</option>
							</select>
						</div>
					</div>
					<div class="4u 12u$(3)">
						<input type="radio" id="ekonomi" name="priority">
						<label for="ekonomi">Ekonomi</label>
					</div>
					<div class="4u 12u$(3)">
						<input type="radio" id="bisnis" name="priority">
						<label for="bisnis">Bisnis</label>
					</div>
					<div class="4u$ 12u$(3)">
						<input type="radio" id="eksekutif" name="priority">
						<label for="eksekutif">Eksekutif</label>
					</div>
					<div class="12u$">
						<ul class="actions" style="float:right">
							<li><input type="reset" value="Hapus" class="special" /></li>
							<li><input type="button" value="Pesan Tiket" id="exambutton" onclick="hasilPesanTiket()" /></li>
						</ul>
					</div>
				</div>
			</form>
		</div>
	</section>

	<!-- Footer (Test) -->
	<?php include "views/footer.php"; ?>

	<!-- JavaScript -->
	<script language="javascript">
		function sorot(x) {
			x.style.background = "rgba(144, 144, 144, 0.7)";
		}

		function hilang(y) {
			y.style.background = "rgba(144, 144, 144, 0.07)";
		}

		var modal = document.getElementById('exammodal');
		var btn = document.getElementById("exambutton");
		var span = document.getElementsByClassName("close")[0];
		span.onclick = function() {
			modal.style.display = "none";
		}

		function hasilPesanTiket() {
			let name = document.getElementById("name").value;
			let email = document.getElementById("email").value;
			let nik = document.getElementById("nik").value;
			let hp = document.getElementById("hp").value;
			let lokasi = document.getElementById("lokasi").value;
			let penumpang = document.getElementById("penumpang").value;
			let waktu = document.getElementById("waktu").value;
			let ekonomi = document.getElementById("ekonomi").checked;
			let bisnis = document.getElementById("bisnis").checked;
			let eksekutif = document.getElementById("eksekutif").checked;
			let errorData = "";
			let harga = 0;

			if (name == "") {
				document.getElementById("name").style = "border: 1px solid red";
				errorData = "name"
			} else if (email == "") {
				document.getElementById("email").style = "border: 1px solid red";
				errorData = "email"
			} else if (nik == "") {
				document.getElementById("nik").style = "border: 1px solid red";
				errorData = "nik"
			} else if (hp == "") {
				document.getElementById("hp").style = "border: 1px solid red";
				errorData = "hp"
			} else if (lokasi == "") {
				document.getElementById("lokasi").style = "border: 1px solid red";
				errorData = "lokasi"
			} else if (penumpang == "") {
				document.getElementById("penumpang").style = "border: 1px solid red";
				errorData = "penumpang"
			} else if (waktu == "") {
				document.getElementById("waktu").style = "border: 1px solid red";
				errorData = "waktu"
			}

			if (errorData != "") {
				window.alert("Data " + errorData + " tidak boleh kosong")

				return;
			}

			if (waktu == 1) {
				harga += 10000;
			} else if (waktu == 2) {
				harga += 15000
			} else if (waktu == 3) {
				harga += 12500
			} else {
				harga += 12000
			}

			if (lokasi == 1) {
				harga += 75000
			} else if (lokasi == 2) {
				harga += 100000
			} else if (lokasi == 3) {
				harga += 80000
			} else {
				harga += 150000
			}

			if (ekonomi == true) {
				harga += 75000
			} else if (bisnis == true) {
				harga += 100000
			} else if (eksekutif == true) {
				harga += 150000
			}

			harga = harga * penumpang

			if (penumpang > 5) {
				harga = harga - 50000
			}

			window.alert("Terima kasih " + name + " sudah melakukan pembelian tiket, total pembelian Anda Rp. " + harga + ", -. Silakan kembali ke halaman utama.")
			window.location.href = "index.php"
		}
	</script>

</body>

</html>