<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<title>Transit Travel Ticket</title>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<link rel="stylesheet" href="css/skel.css" />
	<link rel="stylesheet" href="css/style.css" />
	<link rel="stylesheet" href="css/style-xlarge.css" />

	<link rel="icon" href="images/logo.png">
</head>

<body class="landing">

	<!-- Header -->
	<?php include "views/header.php"; ?>

	<!-- Banner -->
	<section id="banner">
		<h3>Discover Your Journey With Transit Travel</h3>		
		<ul class="actions">
			<li>
				<a href="#" class="button big">Explore Now</a>
			</li>
		</ul>
	</section>

	<!-- One -->
	<section id="two" class="wrapper style2 special">
		<div class="container">
			<header class="major">
				<h2>Sejarah</h2>
				<br>
				<p align="justify">
				Lebih dari 10 tahun kami melayani puluhan ribu penumpang setia Transit Travel, dengan memberikan fasilitas terbaik sehingga pelanggan merasakan kenyamanan ketika berpergian jauh.
				Kini kami memprioritaskan untuk lebih mengupgrade segala hal yang diperlukan agar dapat menjadi yang terbaik bagi nusa bangsa.
				</p>
				<p align="justify">
				
				</p>
			</header>	
		</div>
	</section>		

	<!-- Two -->
	<section id="one" class="wrapper style1 special">
		<div class="container">
			<header class="major">
				<h2>Who We Are</h2>				
			</header>
			<div class="row 150%">
				<div class="4u 12u$(medium)">
					<section class="box">
						<i class="icon big rounded color1 fa-rocket"></i>
						<h3>Kereta Pintar</h3>
						<p>Kereta cepat dengan sistem terbaik yang mampu membuat perjalanan anda lebih aman dari segala kondisi buruk, CCTV di setiap sudut agar terjaminnya keamanan di setiap kereta.</p>
					</section>
				</div>
				<div class="4u 12u$(medium)">
					<section class="box">
						<i class="icon big rounded color1 fa-users"></i>
						<h3>Pegawai</h3>
						<p>Pegawai kami dibekali dengan akhlak yang santun, yang tentu saja mampu membuat perjalanan anda menjadi lebih nyaman.</p>
					</section>
				</div>
				<div class="4u$ 12u$(medium)">
					<section class="box">
						<i class="icon big rounded color1 fa-line-chart"></i>
						<h3>Fasilitas</h3>
						<p>Tempat duduk yang nyaman anda kenakan, mampu membuat anda rileks ketika beristirahat, Toilet anti bau dan kotor, AC dingin yang dapat anda atur sendiri sesuai keinginan anada.</p>
					</section>
				</div>
			</div>
		</div>
	</section>

	<!-- Trhee -->
	<section id="two" class="wrapper style2 special">
		<div class="container">
			<header class="major">
				<h2>Our Team</h2>				
			</header>
			<section class="profiles">
				<div class="row">
					<section class="3u 6u(medium) 12u$(xsmall) profile">
						<img src="images/p1.jpg" alt="user1" width="92px" height="92px" />
						<h4>Sintia Damayanti</h4>
						<p>CEO</p>
					</section>
					<section class="3u 6u$(medium) 12u$(xsmall) profile">
						<img src="images/p2.jpg" alt="user2"  width="92px" height="92px" />
						<h4>Agus Purnomo</h4>
						<p>HRD</p>
					</section>
					<section class="3u 6u(medium) 12u$(xsmall) profile">
						<img src="images/p5.jpg" alt="user3" width="92px" height="92px" />
						<h4>Salsa Masala</h4>
						<p>Director of Technology</p>
					</section>
					<section class="3u$ 6u$(medium) 12u$(xsmall) profile">
						<img src="images/p6.jpg" alt="user4"  width="92px" height="92px" />
						<h4>Muhammad Evan</h4>
						<p>Director of Sales</p>
					</section>
				</div>
			</section>			
		</div>
	</section>

	<!-- Footer -->
	<?php include "views/footer.php"; ?>

</body>

<?php include "script.php"; ?>

</html>