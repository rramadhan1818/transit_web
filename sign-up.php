<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<title>Daftar Akun Transit Travel</title>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<script src="js/init.js"></script>
	<link rel="stylesheet" href="css/skel.css" />
	<link rel="stylesheet" href="css/style.css" />
	<link rel="stylesheet" href="css/style-xlarge.css" />

	<link rel="icon" href="images/logo.png">
</head>

<body>

	<?php include "views/header.php"; ?>

	<!-- One -->
	<section id="three" class="wrapper style1 special">
		<div class="container">
			<header class="major">
				<h2>Daftar Akun Transit Anda Segera</h2>
				<p>Agar Dapat Menikmati Setiap Fitur Dari Transit Travel</p>
			</header>
		</div>
		<div class="container 50%">
			<form action="#" method="post">
				<div class="row uniform">
					<div class="12u">
						<input name="nama" id="nama" value="" placeholder="Nama.." type="text">
					</div>
					<div class="12u">
						<input name="email" id="email" value="" placeholder="Email.." type="email">
					</div>
					<div class="12u">
						<input name="handphone" id="handphone" value="" placeholder="No Handphone.." type="text">
					</div>
					<div class="12u">
						<input name="password" id="password" value="" placeholder="Password.." type="password">
					</div>
					<div class="12u">
						<ul class="actions">
							<li>
								<input value="DAFTAR" class="special big" type="button" onclick="checkError()">
							</li>
						</ul>
					</div>
				</div>
			</form>
		</div>
	</section>

	<!-- Footer (Test) -->
	<?php include "views/footer.php"; ?>

</body>

<script language="javascript">
	function checkError() {
		let nama = document.getElementById("nama").value;
		let email = document.getElementById("email").value;
		let handphone = document.getElementById("handphone").value;
		let password = document.getElementById("password").value;
		let errorData = "";
		let errorPassword = "";

		
		if (nama == "") {
			document.getElementById("nama").style = "border: 1px solid red";
			errorData = "nama"
		}
		
		else if (email == "") {
			document.getElementById("email").style = "border: 1px solid red";
			errorData = "email"
		}

		else if (handphone == "") {
			document.getElementById("handphone").style = "border: 1px solid red";
			errorData = "handphone"
		}

		else if (password == "" ) {
		document.getElementById("password").style = "border: 1px solid red";
		errorData = "password"
		}



		if (errorData != "") {
			window.alert("Data " + errorData + " tidak boleh kosong")
		} else {
			document.getElementById("password").style = "border: 1px solid black";
			document.getElementById("handphone").style = "border: 1px solid black";
			document.getElementById("email").style = "border: 1px solid black";
			document.getElementById("nama").style = "border: 1px solid black";
		}

		redirect('sign-in.php')
	}
</script>

</html>