

<header id="header">
    <h1><a href="index.php">Transit Travel</a></h1>
    <nav id="nav">
        <ul>
            <li><a href="index.php">Beranda</a></li>
            <li><a href="about-us.php">Tentang Kami</a></li>
            <li><a href="ticket.php">Pesan Tiket</a></li>
            <li><a href="facility.php">Fasilitas</a></li>
            <li><a href="feature.php">Fitur</a></li>
            <li>
                <?php 
                    if ($_SESSION['username']){
                        echo "Halo, ".$_SESSION['username'];
                    } else {
                        echo '<a href="sign-in.php" class="button special">Masuk</a>';
                    }
                ?>
            </li>
            <?php 
                if ($_SESSION['username']){
                    echo '<li><a href="" class="button special">Logout</a></li>';
                    session_destroy();
                }
            ?>
           
        </ul>
    </nav>
</header>