<footer id="footer">
    <div class="container">
        <section class="links">
            <div class="row">
                <section class="3u 6u$(medium) 12u$(small)">
                    <h3>Halaman Konten</h3>
                    <ul class="unstyled">
                        <li><a href="index.php">Beranda</a></li>
                        <li><a href="about-us.php">Tentang Kami</a></li>
                        <li><a href="facility.php">Fasilitas</a></li>
                        <li><a href="feature.php">Fitur</a></li>
                        <li><a href="ticket.php">Tiket</a></li>
                    </ul>
                </section>
                <section class="3u 6u$(medium) 12u$(small)">
                    <h3>Destinasi Populer</h3>
                    <ul class="unstyled">
                        <li>Tiket Kereta Api ke Jakarta</li>
                        <li>Tiket Kereta Api ke Bandung</li>
                        <li>Tiket Kereta Api ke Jogja</li>
                        <li>Tiket Kereta Api ke Surabaya</li>
                        <li>Tiket Kereta Api ke Semarang</li>
                    </ul>
                </section>
                <section class="3u 6u(medium) 12u$(small)">
                    <h3>Rute Terpopuler</h3>
                    <ul class="unstyled">
                        <li>Tiket Kereta Api Bandung Jakarta</li>
                        <li>Tiket Kereta Api Semarang Jakarta</li>
                        <li>Tiket Kereta Api Cirebon Jakarta</li>
                        <li>Tiket Kereta Api Malang Jakarta</li>
                        <li>Tiket Kereta Api Bandung Jogja</li>
                    </ul>
                </section>
                <section class="3u$ 6u$(medium) 12u$(small)">
                    <h3>Dukungan</h3>
                    <ul class="unstyled">
                        <li>Pusat Bantuan</li>
                        <li>Kebijakan Privasi</li>
                        <li>Syarat & Ketentuan</li>
                        <li>Group Booking</li>
                        <li>Pengaduan</li>
                    </ul>
                </section>
            </div>
        </section>
        <div class="row">
            <div class="8u 12u$(medium)">
                <ul class="copyright">
                    <li>Copyright Transit Travel &#169; <script type='text/javascript'>
                            var creditsyear = new Date();
                            document.write(creditsyear.getFullYear());
                        </script> <a expr:href='data:blog.homepageUrl'>
                            <data:blog.title /></a>. All rights reserved.</li>
                </ul>
            </div>
            <div class="4u$ 12u$(medium)">
                <ul class="icons">
                    <li>
                        <a class="icon rounded fa-facebook"><span class="label">Facebook</span></a>
                    </li>
                    <li>
                        <a class="icon rounded fa-twitter"><span class="label">Twitter</span></a>
                    </li>
                    <li>
                        <a class="icon rounded fa-google-plus"><span class="label">Google+</span></a>
                    </li>
                    <li>
                        <a class="icon rounded fa-linkedin"><span class="label">LinkedIn</span></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>