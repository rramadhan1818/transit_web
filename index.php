<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<title>Transit Travel Ticket</title>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<link rel="stylesheet" href="css/skel.css" />
	<link rel="stylesheet" href="css/style.css" />
	<link rel="stylesheet" href="css/alert.css" />
	<link rel="stylesheet" href="css/style-xlarge.css" />

	<link rel="icon" href="images/logo.png">
</head>

<body class="landing">

	<!-- Header -->
	<?php include "views/header.php"; ?>

	<!-- Banner -->
	<section id="banner">
		<h3>Hi. Transit Siap Menemani Perjalananmu!</h3>
		<p>Mau pesan tiket kerta ? Pesan aja ga pake ribet.</p>
		<ul class="actions">
			<li>
				<a href="ticket.php" class="button big">Pesan Tiket</a>
			</li>
		</ul>
	</section>

	<!-- One -->
	<section id="one" class="wrapper style1 special">
		<div class="container">
			<header class="major">
				<h2>Kelebihan Memakai Transit</h2>
				<p>Ada berbagai macam fitur tunggul di Transit</p>
			</header>
			<div class="row 150%">
				<div class="4u 12u$(medium)">
					<section class="box">
						<i class="icon big rounded color1 fa-cloud"></i>
						<h3>Asuransi Kereta Api</h3>
						<p>Asuransi perjalanan kereta api memberi kamu perlindungan saat bepergian dengan kereta. Perjalanan sobat tiket jadi aman dan nyaman.</p>
					</section>
				</div>
				<div class="4u 12u$(medium)">
					<section class="box">
						<i class="icon big rounded color9 fa-desktop"></i>
						<h3>Pilih Kursi Bebas</h3>
						<p>Bebas memilih tempat duduk favorit anda untuk perjalanan anda di semua gerbong kereta yang disediakan oleh kereta api indonesia.</p>
					</section>
				</div>
				<div class="4u$ 12u$(medium)">
					<section class="box">
						<i class="icon big rounded color6 fa-rocket"></i>
						<h3>Tersedia Online</h3>
						<p>Pilih jadwal kereta terbaik sesuai dengan kebutuhan Anda. Di sini Anda dapat menemukan semua jadwal kereta dengan tawaran terbaik.</p>
					</section>
				</div>
			</div>
		</div>
	</section>


	<!-- Two -->
	<section id="two" class="wrapper style2 special">
		<div class="container">
			<header class="major">
				<h2>Apa Kata Mereka ?</h2>
				<p>Pendapat dari para pelanggan yang sudah sering memesan di Transit Travel</p>
			</header>
			<section class="profiles">
				<div class="row">
					<section class="3u 6u(medium) 12u$(xsmall) profile">
						<img src="images/p1.jpg" width="92px" height="92px" alt="" />
						<h4>Sintia Damayanti</h4>
						<p>Cara pesan gampang, pembayaran tidak rumit.</p>
					</section>
					<section class="3u 6u$(medium) 12u$(xsmall) profile">
						<img src="images/p2.jpg" width="92px" height="92px" alt="" />
						<h4>Agus Purnomo</h4>
						<p>Tempat duduknya sesuai, tidak tertukar.</p>
					</section>
					<section class="3u 6u(medium) 12u$(xsmall) profile">
						<img src="images/p4.jpg" width="92px" height="92px" alt="" />
						<h4>Salsa Masala</h4>
						<p>Customer servis sangat nyaman dan ramah.</p>
					</section>
					<section class="3u$ 6u$(medium) 12u$(xsmall) profile">
						<img src="images/p3.jpg" width="92px" height="92px" alt="" />
						<h4>Muhammad Evan</h4>
						<p>Pelayanan bagus, cara pembayaran simpel.</p>
					</section>
				</div>
			</section>
			<footer>
				<p>Sebagai perusahaan yang mengelola pemesanan tiket kereta api di Indonesia, Transit Travel telah banyak melayani pemesanan tiket KA, baik Ka Utama (komersil dan Non Komersil), maupun KA Lokal di Jawa dan Sumatera.</p>
				<ul class="actions">
					<li>
						<a href="#" class="button big">Lihat lebih</a>
					</li>
				</ul>
			</footer>
		</div>
	</section>

	<section id="three" class="wrapper style3 special">
		<div class="container">
			<header class="major">
				<h2>Form Komentar</h2>
				<p>Beri kami komentar atau saran untuk perkembangan kami!</p>
			</header>
		</div>
		<div class="container 50%">
			<form action="#" method="post">
				<div class="row uniform">
					<div class="6u 12u$(small)">
						<input name="name" id="name" value="" placeholder="Name" type="text">
					</div>
					<div class="6u$ 12u$(small)">
						<input name="email" id="email" value="" placeholder="Email" type="email">
					</div>
					<div class="12u$">
						<textarea name="message" id="message" placeholder="Message" rows="6"></textarea>
					</div>
					<div class="12u$">
						<ul class="actions">
							<li><input value="Send Message" class="special big" type="button" onclick="submitComment()"></li>
						</ul>
					</div>
				</div>
			</form>
		</div>
	</section>

	<!-- Footer -->
	<?php include "views/footer.php"; ?>

	<script language="javascript">
		function submitComment () {
			let name = document.getElementById('name').value
			let email = document.getElementById('email').value
			let message = document.getElementById('message').value
			let errorData = ""

			if (name == "") {
				document.getElementById("name").style = "border: 1px solid red";
				errorData = "name"
			} else if (email == "") {
				document.getElementById("email").style = "border: 1px solid red";
				errorData = "email"
			} else if (message == "") {
				document.getElementById("message").style = "border: 1px solid red";
				errorData = "message"
			}

			if (errorData != "") {
				window.alert("Data " + errorData + " tidak boleh kosong")

				return;
			}

			window.alert("Terima kasih sudah memberikan masukan kepada kami. Kami akan berusaha semaksimal mungkn untuk memenuhi kebutuhan Anda.")
			window.location.href = "index.php"
		}
	</script>

</body>

<?php include "script.php"; ?>

</html>