<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<title>Fitur</title>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<script src="js/init.js"></script>
	<link rel="stylesheet" href="css/skel.css" />
	<link rel="stylesheet" href="css/style.css" />
	<link rel="stylesheet" href="css/style-xlarge.css" />

	<link rel="icon" href="images/logo.png">
</head>

<body>

	<?php include "views/header.php"; ?>

	<section id="main" class="wrapper">
		<div class="container">

			<header class="major">
				<h2>Transit Travel</h2>
				<p>Cek Tiket Kereta Api Ke Destinasi Favoritmu!</p>
			</header>

			<a href="#" class="image fit"><img src="images/image1.jpeg" alt="" /></a>
			<p>Kamu ingin berangkat liburan atau pulang kampung, tapi belum tahu jadwal kereta api menuju ke kota tujuan kamu? Tenang saja, kamu tidak perlu bingung karena di Transit Travel, kamu bisa mengecek tiket kereta api ke destinasi yang kamu inginkan. Cara mengecek tiket kereta api online di Transit Travel ini pun sangat mudah. Kamu hanya perlu memasukkan destinasi tujuan kamu, tanggal perjalanan yang kamu pilih, sekaligus jumlah penumpang yang akan kamu pesankan tiket KAI online di Transit Travel. Ketika kamu klik cari kereta, maka kamu akan segera menemukan jadwal kereta yang tersedia, lengkap dengan ketersediaan tiket yang bisa kamu booking. Jadwal kereta di Transit Travel sangat lengkap dan bervariasi, sehingga ada banyak pilihan untuk kamu yang ingin pesan tiket kereta api ke kota pilihan kamu.</p>
			<p>Jadwal tiket kereta yang tersedia di Transit Travel ditampilkan lengkap dengan hari, jam, dan tanggal keberangkatan kereta. Semakin mudah bukan untuk kamu memilih tiket KAI online yang paling sesuai? Tidak hanya itu, proses untuk membeli tiket kereta online di Transit Travel juga semakin mudah karena kamu bisa mengecek tiket kereta pulang pergi (round trip) dan mendapatkan tiket PP tersebut hanya dalam sekali booking.Untuk mengecek ketersediaan tiket kereta api PP ini, kamu hanya tinggal memasukkan tanggal keberangkatan (tanggal pergi) dan tanggal kembali. Tidak perlu menunggu lama, hasil pencarian kereta kamu akan langsung ditampilkan lengkap dengan waktu keberangkatan dan waktu sampai di tempat tujuan.</p>

			<header class="major">
				<h3>Promo Tiket Kereta Api untuk Liburan Seru</h3>
			</header>
			<p>Salah satu hal yang tentunya perlu untuk diperhatikan saat liburan adalah soal budget. Terkadang, rencana untuk liburan ke destinasi yang kamu inginkan menjadi terkendala karena masalah budget. Padahal, ada banyak hal yang bisa kamu lakukan untuk membuat liburan kamu tidak terhalang oleh hal yang satu ini. Salah satu cara untuk tetap liburan tanpa harus pusing soal bujet adalah mencari transportasi yang murah tapi tetap nyaman.Untuk itu, kamu bisa pesan tiket PT KAI di Transit Travel. Sebagai partner liburan kamu yang menyenangkan, dengan promo Transit Travel membantumu untuk booking tiket kereta dengan mudah, cepat dan murah. Selain menyediakan harga tiket kereta api yang terjangkau, Transit Travel juga menyediakan banyak promo tiket kereta yang asyik untuk menjadikan liburan kamu dengan kereta api menjadi semakin berkesan.</p>
			<p>Promo tiket kereta api di Transit Travel dapat kamu temukan di website maupun di aplikasi. Ada diskon spesial untuk kamu yang ingin melakukan perjalanan dengan kereta api. Makanya, kamu tidak perlu ragu untuk pesan tiket kereta api di Transit Travel. Berbagai macam promo tiket kereta api ini akan membuat bujet liburan kamu semakin hemat dan menyenangkan.Cari promo tiket kereta api murah hanya di Transit Travel. Pemesanan melalui website dan aplikasi sama-sama bisa membuat kamu mendapatkan promo tiket kereta murah di Transit Travel. Di mana lagi kamu bisa menemukan kemudahan seperti ini? Karena itu, kamu tidak perlu ragu untuk memilih Transit Travel sebagai partner liburan kamu yang istimewa.Beli tiket kereta api online di Transit Travel dan dapatkan promo tiket kereta api untuk liburan seru kamu yang semakin hemat. Banyak diskon istimewa untuk kamu yang ingin liburan menggunakan moda transportasi populer yang satu ini.Pilih tiket kereta api ke destinasi yang kamu mau sekarang juga untuk mewujudkan liburan impian kamu bersama teman-teman maupun bersama seluruh keluarga. Transit Travel menyediakan banyak pilihan destinasi kereta api dan harga tiket kereta api yang bisa kamu pilih sesuai dengan bujet maupun kebutuhan kamu. Jangan ragu untuk pesan tiket kereta apimu sekarang hanya di Transit Travel!</p>

		</div>
	</section>

	<!-- Footer -->
	<?php include "views/footer.php"; ?>

</body>

</html>